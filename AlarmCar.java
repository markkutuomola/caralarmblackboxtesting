package assignment3;

import java.util.Random;

import junit.framework.Assert;
import nz.ac.waikato.modeljunit.Action;
import nz.ac.waikato.modeljunit.FsmModel;
import nz.ac.waikato.modeljunit.GreedyTester;
import nz.ac.waikato.modeljunit.Tester;
import nz.ac.waikato.modeljunit.coverage.ActionCoverage;
import nz.ac.waikato.modeljunit.coverage.CoverageMetric;
import nz.ac.waikato.modeljunit.coverage.StateCoverage;
import nz.ac.waikato.modeljunit.coverage.TransitionCoverage;
import nz.ac.waikato.modeljunit.coverage.TransitionPairCoverage;

//import theft_alarm_v3.Alarm;

public class AlarmCar implements FsmModel {
	private Random rand= new Random();
	private theft_alarm_v3.Alarm ac = new theft_alarm_v3.Alarm();
	
	private boolean buttonON;
	private boolean buttonOFF;
	private boolean doorOPEN;
	private boolean doorsCLOSED;
	
	private State state;
	
	private enum State {
		DISARMED,
		ARMED,
		ALARM;
	}

	@Override
	public Object getState() {
		return (String.valueOf(state));
	}
	
	@Override
	public void reset (boolean arg0) {
		state = State.DISARMED;
		buttonON = false;
		buttonOFF = false;
		doorOPEN = false;
		doorsCLOSED = false;
		ac.reset();
	}
	
	
	public boolean idleGuard() {
		return (state == State.DISARMED && !getButtonON(false) && !getDoorsCLOSED(false) && !getButtonOFF(false) && !getDoorOPEN(false));}
	
	@Action
	public void idle() {
		state = State.DISARMED;
		Assert.assertEquals(0, ac.button_ON()); //should blink 0 times, covers P5 = the idle loop
	}
	
	
	public boolean disarmedToArmedGuard() {
		return (state == State.DISARMED && getButtonON(true) && getDoorsCLOSED(true) && !getButtonOFF(false) && !getDoorOPEN(false));}
	
	@Action
	public void disarmedToArmed() {
		state = State.ARMED;
		Assert.assertEquals(1, ac.button_ON()); //should blink 1 time, covers P1 = transition from disarmed state to armed state
	}
	
	
	public boolean armedToDisarmedGuard() {
		return (state == State.ARMED && getButtonON(true) && getDoorsCLOSED(true) && getButtonOFF(true) && !getDoorOPEN(false));}
	
	@Action
	public void armedToDisarmed() {
		buttonON = false;
		state = State.DISARMED;
		Assert.assertEquals(2, ac.button_OFF()); //should blink 2 times, covers P2 = transition from armed state to disarmed state
	}
	
	public boolean armedToAlarmGuard() {
		return (state == State.ARMED && getButtonON(true) && getDoorsCLOSED(true) && !getButtonOFF(false) && getDoorOPEN(true));}
	
	@Action
	public void armedToAlarm() {
		state = State.ALARM;
		Assert.assertEquals(27, ac.doorOPENED()); //should blink 27 times, covers P3 = transition from armed state to alarm state
	}
	
	
	public boolean alarmToDisarmedGuard() {
		return (state == State.ALARM && getButtonON(true) && getDoorsCLOSED(true) && getButtonOFF(true) && getDoorOPEN(true));}
	
	@Action
	public void alarmToDisarmed() {
		state = State.DISARMED;
		Assert.assertEquals(2, ac.button_OFF()); //should blink 2 times, covers p4 = transition from alarm state to disarm state
	}
	
	
	public boolean getDoorOPEN(boolean value){
		//Random r = new Random();
		//int i = r.nextInt(2);
		//return (i==0?false:true);
		doorOPEN = value;
		return doorOPEN;
	}
	public boolean getButtonOFF(boolean value){
		buttonOFF = value;
		return buttonOFF;
	}
	
	public boolean getButtonON(boolean value){
		buttonON = value;
		return buttonON;
	}
	
	public boolean getDoorsCLOSED(boolean value){
		doorsCLOSED = value;
		return doorsCLOSED;
	}
	
	public static void main(String[] args) {
		Tester tester = new GreedyTester(new AlarmCar());
		System.out.println("------------------------");
		tester.setRandom(new Random());
		
		CoverageMetric trCoverage = new TransitionCoverage();
		tester.addListener(trCoverage);
		CoverageMetric actionCoverage = new ActionCoverage();
		tester.addListener(actionCoverage);
		CoverageMetric tpCoverage = new TransitionPairCoverage();
		tester.addListener(tpCoverage);
		CoverageMetric stCoverage = new StateCoverage();
		tester.addListener(stCoverage);
		
		tester.addListener("verbose");
		
		int steps = 0;
		while(tpCoverage.getPercentage() < 100 ) {
			tester.generate();
			steps++;
		}
		System.out.println("Generated "+steps+" steps.");
		tester.printCoverage();
		
	}
}
